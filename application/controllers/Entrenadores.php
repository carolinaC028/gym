<?php
class Entrenadores extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Entrenador");
		if(!$this->session->userdata("conectado")){
		redirect("welcome/login");
	}
}
	public function index()
	{
		$data["listadoEntrenadores"]=$this->Entrenador->obtenerTodo();
		$this->load->view('header');
		$this->load->view('entrenadores/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('entrenadores/nuevo');
		$this->load->view('footer');
  }
	public function guardarEntrenadores(){
		$datosNuevo=array(
      "nombre_ent"=>$this->input->post("nombre_ent"),
      "apellido_ent"=>$this->input->post("apellido_ent"),
      "correo_ent"=>$this->input->post("correo_ent"),
      "telefono_ent"=>$this->input->post("telefono_ent"),
      "direccion_ent"=>$this->input->post("direccion_ent"),
		);
		// print_r($datosNuevo);
		if ($this->Entrenador->insertar($datosNuevo)) {
			  $this->session->set_flashdata("confirmacion","Entrenador guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			 $this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('entrenadores/index');
		}
		public function eliminar($id_ent){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
                $this->session->set_flashdata("error","No tiene permiso para eliminar");
                redirect("entrenadores/index");
            };
		     if ($this->Entrenador->eliminarPorId($id_ent)) {
				$this->session->set_flashdata("confirmacion","Entrenador eliminado existosamente");
		} else {
 				$this->session->set_flashdata("error","Error al eliminar intente de nuevo");
		}
		redirect('entrenadores/index');
	}
	public function editar($id_ent){
            $data["entrenadorEditar"]=$this->Entrenador->obtenerPorId($id_ent);
            $this->load->view('header');
            $this->load->view('entrenadores/editar',$data);
            $this->load->view('footer');
        }
        ////actualizar
        public function procesarActualizaciones(){
					$datosEditados=array(
						"nombre_ent"=>$this->input->post("nombre_ent"),
						"apellido_ent"=>$this->input->post("apellido_ent"),
						"correo_ent"=>$this->input->post("correo_ent"),
						"telefono_ent"=>$this->input->post("telefono_ent"),
						"direccion_ent"=>$this->input->post("direccion_ent"),
					);
        $id_ent=$this->input->post("id_ent");
        if($this->Entrenador->actualizar($id_ent,$datosEditados)){
					$this->session->set_flashdata("confirmacion","Entrenador actualizado existosamente");
						redirect("entrenadores/index");
        }else{
								$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
            // echo "ERROR AL ACTULIZAR :(";
        }
				redirect("clientes/index");
        }

}//no cerrar
