<?php
class Rutinas extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Rutina");

	}
	public function index()
	{
		$data["listadoRutinas"]=$this->Rutina->obtenerTodo();
		$this->load->view('header');
		$this->load->view('rutinas/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('rutinas/nuevo');
		$this->load->view('footer');
  }
	public function guardarRutinas(){
		$datosNuevo=array(
      "tipo_rut"=>$this->input->post("tipo_rut"),
      "duracion_rut"=>$this->input->post("duracion_rut"),
      "detalle_rut"=>$this->input->post("detalle_rut"),
      "dia_rut"=>$this->input->post("dia_rut"),
		);
		// print_r($datosNuevo);
		if ($this->Rutina->insertar($datosNuevo)) {
			$this->session->set_flashdata("confirmacion","Rutina guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			$this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('rutinas/index');
		}
		public function eliminar($id_rut){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
                $this->session->set_flashdata("error","No tiene permiso para eliminar");
                redirect("rutinas/index");
            };
		     if ($this->Rutina->eliminarPorId($id_rut)) {
				$this->session->set_flashdata("confirmacion","Rutina eliminado existosamente");
		} else {
 				$this->session->set_flashdata("error","Error al eliminar intente de nuevo");
		}
		redirect('rutinas/index');
	}
	public function editar($id_rut){
            $data["rutinaEditar"]=$this->Rutina->obtenerPorId($id_rut);
            $this->load->view('header');
            $this->load->view('rutinas/editar',$data);
            $this->load->view('footer');
    }
        ////actualizar
        public function procesarActualizaciones(){
          $datosEditados=array(
            "tipo_rut"=>$this->input->post("tipo_rut"),
            "duracion_rut"=>$this->input->post("duracion_rut"),
            "detalle_rut"=>$this->input->post("detalle_rut"),
            "dia_rut"=>$this->input->post("dia_rut"),
      		);
        $id_rut=$this->input->post("id_rut");
        if($this->Rutina->actualizar($id_rut,$datosEditados)){
					$this->session->set_flashdata("confirmacion","Rutina actualizado existosamente");
        }else{
					$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
					// echo "ERROR AL ACTULIZAR :(";
          }
							redirect("rutinas/index");
        }
}//no cerrar
?>
