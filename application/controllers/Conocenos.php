<?php
class Conocenos extends CI_Controller {
	public function __construct(){
		parent:: __construct();
	}
  public function nosotros(){
    $this->load->view('header');
    $this->load->view('clientes/nosotros');
    $this->load->view('footer');
  }
	public function bienvenido(){
	$this->load->view('header');
	$this->load->view('conocenos/bienvenido');
	$this->load->view('footer');
	}
	public function servicio(){
	$this->load->view('header');
	$this->load->view('conocenos/servicio');
	$this->load->view('footer');
	}
	public function elegir(){
	$this->load->view('header');
	$this->load->view('conocenos/elegir');
	$this->load->view('footer');
	}
}
?>
