<?php
class Horarios extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Horario");
		if(!$this->session->userdata("conectado")){
		redirect("welcome/login");
	}
	}
	public function index()
	{
		$data["listadoHorarios"]=$this->Horario->obtenerTodo();
		$this->load->view('header');
		$this->load->view('horarios/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('horarios/nuevo');
		$this->load->view('footer');
  }
	public function guardarHorarios(){
		$datosNuevo=array(
      "dia_hor"=>$this->input->post("dia_hor"),
      "hora_hor"=>$this->input->post("hora_hor"),
      "tipo_hor"=>$this->input->post("tipo_hor"),
		);
		// print_r($horariosNuevo);
		if ($this->Horario->insertar($datosNuevo)) {
			$this->session->set_flashdata("confirmacion","Horarios guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			$this->session->set_flashdata("error","Error al guardar horario intente de nuevo");

      // echo "<h1>Error al insertar</h1>";
		}
		redirect('horarios/index');
		}

<<<<<<< HEAD
		public function eliminar($id_hor){
						if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
								$this->session->set_flashdata("error","No tiene permiso para eliminar");
								redirect("horarios/index");
						};
				if ($this->Horario->eliminarPorId($id_hor)) {
			 $this->session->set_flashdata("confirmacion","Horario eliminado existosamente");
	 } else {
				 $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
	 }
	 redirect('horarios/index');
 }
=======
	   public function borrar($id_hor){
		     if ($this->horario->borrar($id_hor)) {
					 $this->session->set_flashdata("confirmacion","Horario eliminado existosamente");
		} else {
			$this->session->set_flashdata("error","Error al eliminar horario intente de nuevo");
		}
		redirect('horarios/index');
	}
>>>>>>> a85939b648400387930f865a0c7660cdc44e8b6f
	//FUNCION REEDERIZAR VISTA EDITAR
	public function editar($id_hor){
		$data ["horarioEditar"]=$this->Horario->obtenerPorId($id_hor);
		$this->load->view('header')	;
		$this->load->view('horarios/editar',$data);
		$this->load->view('footer')	;
	}

	//PROCESO DE ACTUALIZACION
	public function procesarActualizacion(){
		$horariosEditados=array(
      "dia_hor"=>$this->input->post("dia_hor"),
      "hora_hor"=>$this->input->post("hora_hor"),
      "tipo_hor"=>$this->input->post("tipo_hor"),
		);
		$id_hor=$this->input->post("id_hor");
		if ($this->Horario->actualizar($id_hor,$horariosEditados)) {
			$this->session->set_flashdata("confirmacion","Horario actualizado existosamente");
		} else {
			$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
		}
			redirect("horarios/index");
			// echo "EROOR AL ACTUALIZAR";
		}

}//cierre de la clase
