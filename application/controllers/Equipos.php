<?php
class Equipos extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Equipo");
		if(!$this->session->userdata("conectado")){
		redirect("welcome/login");
	}
	}
	public function index()
	{
		$data["listadoEquipos"]=$this->Equipo->obtenerTodo();
		$this->load->view('header');
		$this->load->view('equipos/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('equipos/nuevo');
		$this->load->view('footer');
  }
  public function guardarEquipos(){
		$datosNuevo=array(
      "estado_equi"=>$this->input->post("estado_equi"),
      "nombre_equi"=>$this->input->post("nombre_equi"),
      "marca_equi"=>$this->input->post("marca_equi"),
      "descripcion_equi"=>$this->input->post("descripcion_equi"),
		);
		// print_r($datosNuevo);
		if ($this->Equipo->insertar($datosNuevo)) {
			$this->session->set_flashdata("confirmacion","Equipo guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			$this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('equipos/index');
		}

		public function eliminar($id_equi){
						if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
								$this->session->set_flashdata("error","No tiene permiso para eliminar");
								redirect("equipos/index");
						};
				if ($this->Equipo->eliminarPorId($id_equi)) {
			 $this->session->set_flashdata("confirmacion","Equipo eliminado existosamente");
	 } else {
				 $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
	 }
	 redirect('equipos/index');
 }
	public function editar($nombre_equi){
            $data["equipoEditar"]=$this->Equipo->obtenerPorId($nombre_equi);
            $this->load->view('header');
            $this->load->view('equipos/editar',$data);
            $this->load->view('footer');
        }
        ////actualizar
        public function procesarActualizaciones(){
					$datosEditados=array(
			      "estado_equi"=>$this->input->post("estado_equi"),
			      "nombre_equi"=>$this->input->post("nombre_equi"),
			      "marca_equi"=>$this->input->post("marca_equi"),
			      "descripcion_equi"=>$this->input->post("descripcion_equi"),
					);
        $nombre_equi=$this->input->post("nombre_equi");
        if($this->Equipo->actualizar($nombre_equi,$datosEditados)){
					$this->session->set_flashdata("confirmacion","Equipo actualizado existosamente");
        }else{
					$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
            // echo "ERROR AL ACTULIZAR :(";
        }
				redirect("equipos/index");
        }

}//NO CERRAR
