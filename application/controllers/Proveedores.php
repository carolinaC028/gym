<?php
class Proveedores extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Proveedor");
		if(!$this->session->userdata("conectado")){
		redirect("welcome/login");
	}
	}
	public function index()
	{
		$data["listadoProveedores"]=$this->Proveedor->obtenerTodo();
		$this->load->view('header');
		$this->load->view('proveedores/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('proveedores/nuevo');
		$this->load->view('footer');
  }
	public function guardarProveedores(){
		$datosNuevo=array(
      "cedula_prov"=>$this->input->post("cedula_prov"),
      "nombre_prov"=>$this->input->post("nombre_prov"),
      "apellido_prov"=>$this->input->post("apellido_prov"),
      "direccion_prov"=>$this->input->post("direccion_prov"),
		);
		// print_r($datosNuevo);
		if ($this->Proveedor->insertar($datosNuevo)) {
			$this->session->set_flashdata("confirmacion","Proveedor guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			$this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('proveedores/index');
		}
		public function eliminar($id_prov){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
                $this->session->set_flashdata("error","No tiene permiso para eliminar");
                redirect("proveedores/index");
            };
		     if ($this->Proveedor->eliminarPorId($id_prov)) {
				$this->session->set_flashdata("confirmacion","Proveedor eliminado existosamente");
		} else {
 				$this->session->set_flashdata("error","Error al eliminar intente de nuevo");
		}
		redirect('proveedores/index');
	}
	public function editar($id_prov){
            $data["proveedorEditar"]=$this->Proveedor->obtenerPorId($id_prov);
            $this->load->view('header');
            $this->load->view('proveedores/editar',$data);
            $this->load->view('footer');
        }
        ////actualizar
        public function procesarActualizaciones(){
          $datosEditados=array(
            "cedula_prov"=>$this->input->post("cedula_prov"),
            "nombre_prov"=>$this->input->post("nombre_prov"),
            "apellido_prov"=>$this->input->post("apellido_prov"),
            "direccion_prov"=>$this->input->post("direccion_prov"),
      		);
        $id_prov=$this->input->post("id_prov");
        if($this->Proveedor->actualizar($id_prov,$datosEditados)){
					$this->session->set_flashdata("confirmacion","Proveedor actualizado existosamente");
        }else{
					$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
            // echo "ERROR AL ACTULIZAR :(";
        }
				redirect("proveedores/index");
        }

}//no cerrar
