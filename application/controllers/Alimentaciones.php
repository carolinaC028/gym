<?php
class Alimentaciones extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Alimentacion");
	}
	public function index()
	{
		$data["listadoAlimentaciones"]=$this->Alimentacion->obtenerTodo();
		$this->load->view('header');
		$this->load->view('alimentaciones/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('alimentaciones/nuevo');
		$this->load->view('footer');
  }
	public function guardarAlimentaciones(){
		$datosNuevo=array(
      "dia_plan"=>$this->input->post("dia_plan"),
      "tipo_plan"=>$this->input->post("tipo_plan"),
      "comida_plan"=>$this->input->post("comida_plan"),
      "detalle_plan"=>$this->input->post("detalle_plan"),
      "vasos_plan"=>$this->input->post("vasos_plan"),
		);
		// print_r($datosNuevo);
		if ($this->Alimentacion->insertar($datosNuevo)) {
			 $this->session->set_flashdata("confirmacion","Plan de Alimentacion guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			 $this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('alimentaciones/index');
		}
		public function eliminar($id_plan){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
                $this->session->set_flashdata("error","No tiene permiso para eliminar");
                redirect("alimentaciones/index");
            };
		     if ($this->Alimentacion->eliminarPorId($id_plan)) {
				$this->session->set_flashdata("confirmacion","Alimentacion eliminado existosamente");
		} else {
 				$this->session->set_flashdata("error","Error al eliminar intente de nuevo");
		}
		redirect('alimentaciones/index');
	}

	//FUNCION REEDERIZAR VISTA EDITAR
	public function editar($id_plan){
		$data ["alimentacionEditar"]=$this->Alimentacion->obtenerPorId($id_plan);
		$this->load->view('header')	;
		$this->load->view('alimentaciones/editar',$data);
		$this->load->view('footer')	;
	}

	//PROCESO DE ACTUALIZACION
	public function procesarActualizacion(){
		$alimentacionEditados=array(
      "dia_plan"=>$this->input->post("dia_plan"),
      "tipo_plan"=>$this->input->post("tipo_plan"),
      "comida_plan"=>$this->input->post("comida_plan"),
      "detalle_plan"=>$this->input->post("detalle_plan"),
      "vasos_plan"=>$this->input->post("vasos_plan"),
		);
		$id_plan=$this->input->post("id_plan");
		if ($this->Alimentacion->actualizar($id_plan,$alimentacionEditados)) {
			$this->session->set_flashdata("confirmacion","Plan de alimentacion actualizado existosamente");
			redirect("alimentaciones/index");
		} else {
				$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
			// echo "EROOR AL ACTUALIZAR";
		}
		redirect("alimentaciones/index");
	}

}//cierre de la clase
