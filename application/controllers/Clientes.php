<?php
class Clientes extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Cliente");
		if(!$this->session->userdata("conectado")){
    redirect("welcome/login");
  }
	}
	public function index()
	{
		$data["listadoClientes"]=$this->Cliente->obtenerTodo();
		$this->load->view('header');
		$this->load->view('clientes/index',$data);
    $this->load->view('footer');
	}

  public function nuevo(){
    $this->load->view('header');
		$this->load->view('clientes/nuevo');
		$this->load->view('footer');
  }
  public function nosotros(){
    $this->load->view('header');
    $this->load->view('clientes/nosotros');
    $this->load->view('footer');
  }
	public function bienvenido(){
	$this->load->view('header');
	$this->load->view('clientes/bienvenido');
	$this->load->view('footer');
	}
	public function servicio(){
	$this->load->view('header');
	$this->load->view('clientes/servicio');
	$this->load->view('footer');
	}
	public function elegir(){
	$this->load->view('header');
	$this->load->view('clientes/elegir');
	$this->load->view('footer');
	}
	public function guardarClientes(){
		$datosNuevo=array(
      "nombre_clie"=>$this->input->post("nombre_clie"),
      "cedula_clie"=>$this->input->post("cedula_clie"),
      "apellido_clie"=>$this->input->post("apellido_clie"),
      "telefono_clie"=>$this->input->post("telefono_clie"),
      "correo_clie"=>$this->input->post("correo_clie"),
      "direccion_clie"=>$this->input->post("direccion_clie"),
		);
		// print_r($datosNuevo);
		if ($this->Cliente->insertar($datosNuevo)) {
			  $this->session->set_flashdata("confirmacion","Cliente guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			  $this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('clientes/index');
		}
		public function eliminar($id_clie){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
                $this->session->set_flashdata("errore","No tiene permiso para eliminar");
                redirect("clientes/index");
            };
		     if ($this->Cliente->eliminarPorId($id_clie)) {
				$this->session->set_flashdata("confirmacion","Cliente eliminado existosamente");
		} else {
 				$this->session->set_flashdata("error","Error al eliminar intente de nuevo");
		}
		redirect('clientes/index');
	}
	public function editar($id_clie){
            $data["clienteEditar"]=$this->Cliente->obtenerPorId($id_clie);
            $this->load->view('header');
            $this->load->view('clientes/editar',$data);
            $this->load->view('footer');
        }
	public function procesarActualizaciones(){
		$datosEditados=array(
      "nombre_clie"=>$this->input->post("nombre_clie"),
      "cedula_clie"=>$this->input->post("cedula_clie"),
      "apellido_clie"=>$this->input->post("apellido_clie"),
      "telefono_clie"=>$this->input->post("telefono_clie"),
      "correo_clie"=>$this->input->post("correo_clie"),
      "direccion_clie"=>$this->input->post("direccion_clie"),
		);
	$id_clie=$this->input->post("id_clie");
	if($this->Cliente->actualizar($id_clie,$datosEditados)){
			$this->session->set_flashdata("confirmacion","Cliente actualizado existosamente");
			redirect("clientes/index");
	}else{
			$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
			// echo "ERROR AL ACTULIZAR :(";
	}
		redirect("clientes/index");
}

}//no cerrar
