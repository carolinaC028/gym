<?php
class Limpiezas extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Limpieza");
		if(!$this->session->userdata("conectado")){
		redirect("welcome/login");
	}
	}

	public function index()
	{
		$data["listadoPersonales"]=$this->Limpieza->obtenerTodo();
		$this->load->view('header');
		$this->load->view('limpiezas/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('limpiezas/nuevo');
		$this->load->view('footer');
  }
	public function guardarPersonal(){
		$datosNuevo=array(
      "horario_limp"=>$this->input->post("horario_limp"),
      "nombre_limp"=>$this->input->post("nombre_limp"),
      "apellido_limp"=>$this->input->post("apellido_limp"),
      "telefono_limp"=>$this->input->post("telefono_limp"),

		);
		// print_r($datosNuevo);
		if ($this->Limpieza->insertar($datosNuevo)) {
			 $this->session->set_flashdata("confirmacion","Personal guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			$this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('limpiezas/index');
		}
		public function eliminar($id_limp){
            if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
                $this->session->set_flashdata("error","No tiene permiso para eliminar");
                redirect("limpiezas/index");
            };
		     if ($this->Limpieza->eliminarPorId($id_limp)) {
				$this->session->set_flashdata("confirmacion","Limpieza eliminado existosamente");
		} else {
 				$this->session->set_flashdata("error","Error al eliminar intente de nuevo");
		}
		redirect('limpiezas/index');
	}
	public function editar($id_limp){
            $data["limpiezaEditar"]=$this->Limpieza->obtenerPorId($id_limp);
            $this->load->view('header');
            $this->load->view('limpiezas/editar',$data);
            $this->load->view('footer');
        }
        ////actualizar
        public function procesarActualizaciones(){
					$datosEditados=array(
            "horario_limp"=>$this->input->post("horario_limp"),
            "nombre_limp"=>$this->input->post("nombre_limp"),
            "apellido_limp"=>$this->input->post("apellido_limp"),
            "telefono_limp"=>$this->input->post("telefono_limp"),
					);
        $id_limp=$this->input->post("id_limp");
        if($this->Limpieza->actualizar($id_limp,$datosEditados)){
					 $this->session->set_flashdata("confirmacion","Personal guardado existosamente");

        }else{
					$this->session->set_flashdata("error","Error al alcualizar intente de nuevo");
            // echo "ERROR AL ACTULIZAR :(";
        }
				redirect("limpiezas/index");
        }

}//no cerrar
