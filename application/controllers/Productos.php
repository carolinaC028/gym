<?php
class Productos extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model("Producto");
		if(!$this->session->userdata("conectado")){
		redirect("welcome/login");
	}
	}
	public function index()
	{
		$data["listadoProductos"]=$this->Producto->obtenerTodo();
		$this->load->view('header');
		$this->load->view('productos/index',$data);
    $this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('productos/nuevo');
		$this->load->view('footer');
  }
	public function guardarProductos(){
		$datosNuevo=array(
      "nombre_pro"=>$this->input->post("nombre_pro"),
      "detalle_pro"=>$this->input->post("detalle_pro"),
      "precio_pro"=>$this->input->post("precio_pro"),
      "marca_pro"=>$this->input->post("marca_pro"),
		);
		// print_r($datosNuevo);
		if ($this->Producto->insertar($datosNuevo)) {
			$this->session->set_flashdata("confirmacion","Producto guardado existosamente");
      // echo "<h1>informacion ingresada</h1>";
		}else{
			$this->session->set_flashdata("error","Error al guardar intente de nuevo");
      // echo "<h1>Error al insertar</h1>";
		}
		redirect('productos/index');
		}

		public function eliminar($id_pro){
						if ($this->session->userdata("conectado")->perfil_usu!="ADMINISTRADOR"){
								$this->session->set_flashdata("error","No tiene permiso para eliminar");
								redirect("productos/index");
						};
				if ($this->Producto->eliminarPorId($id_pro)) {
			 $this->session->set_flashdata("confirmacion","Producto eliminado existosamente");
	 } else {
				 $this->session->set_flashdata("error","Error al eliminar intente de nuevo");
	 }
	 redirect('productos/index');
 }
	//FUNCION REEDERIZAR VISTA EDITAR
	public function editar($id_pro){
		$data ["productoEditar"]=$this->Producto->obtenerPorId($id_pro);
		$this->load->view('header')	;
		$this->load->view('productos/editar',$data);
		$this->load->view('footer')	;
	}

	//PROCESO DE ACTUALIZACION
	public function procesarActualizacion(){
		$datosEditados=array(
			"nombre_pro"=>$this->input->post("nombre_pro"),
			"detalle_pro"=>$this->input->post("detalle_pro"),
			"precio_pro"=>$this->input->post("precio_pro"),
			"marca_pro"=>$this->input->post("marca_pro"),
		);
		$id_pro=$this->input->post("id_pro");
		if ($this->Producto->actualizar($id_pro,$datosEditados)) {
			$this->session->set_flashdata("confirmacion","Producto actualizado existosamente");
		} else {
			$this->session->set_flashdata("error","Error al actualizar intente de nuevo");
			// echo "EROOR AL ACTUALIZAR";
		}
			redirect("productos/index");
	}

}//cierre de la clase
