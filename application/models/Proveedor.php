<?php
  class Proveedor extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("proveedor",$datos);
    }
    public function obtenerTodo(){
      $Proveedores=$this->db->get('proveedor');
      if ($Proveedores->num_rows()>0) {
        return $Proveedores->result();
      } else {
        return false;
      }
    }
    function obtenerPorId($id_prov){
        $this->db->where("id_prov",$id_prov);
        $proveedor=$this->db->get("proveedor");
        if($proveedor->num_rows()>0){
            return $proveedor->row();
        }
        return false;
    }
    public function eliminarPorId($id){
      $this->db->where("id_prov",$id);
      return $this->db->delete('proveedor');
    }
    function actualizar($id_prov,$datos){
        $this->db->where("id_prov",$id_prov);
        return $this->db->update('proveedor',$datos);
    }
}//cierre llave
