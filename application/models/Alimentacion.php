<?php
  class Alimentacion extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("alimentacion",$datos);
    }
    public function obtenerTodo(){
      $alimentaciones=$this->db->get('alimentacion');
      if ($alimentaciones->num_rows()>0) {
        return $alimentaciones->result();
      } else {
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where("id_plan",$id);
      return $this->db->delete('alimentacion');
    }
    //FUNCION PARA CONSULTAR
  function obtenerPorId($id_plan){
    $this->db->where("id_plan", $id_plan);
    $alimentacion=$this->db->get("alimentacion");
    if ($alimentacion->num_rows()>0) {
      return $alimentacion->row();
    } else {
      return false;
    }
  }
    //FUNCION PARA ACTUALIZAR
    function actualizar($id_plan,$data){
      $this->db->where("id_plan",$id_plan);
      return $this->db->update('alimentacion',$data);
    }
}//cierre de la clase
