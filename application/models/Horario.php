<?php
  class Horario extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("horario",$datos);
    }
    public function obtenerTodo(){
      $horarios=$this->db->get('horario');
      if ($horarios->num_rows()>0) {
        return $horarios->result();
      } else {
        return false;
      }
    }
    public function eliminarPorId($id){
      $this->db->where("id_hor",$id);
      return $this->db->delete('horario');
    }
    //FUNCION PARA CONSULTAR
  function obtenerPorId($id_hor){
    $this->db->where("id_hor", $id_hor);
    $horario=$this->db->get("horario");
    if ($horario->num_rows()>0) {
      return $horario->row();
    } else {
      return false;
    }
  }
    //FUNCION PARA ACTUALIZAR
    function actualizar($id_hor,$data){
      $this->db->where("id_hor",$id_hor);
      return $this->db->update('horario',$data);
    }
}//cierre de la clase
