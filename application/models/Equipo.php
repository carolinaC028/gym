<?php
  class Equipo extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("equipo",$datos);
    }
    public function obtenerTodo(){
      $equipos=$this->db->get('equipo');
      if ($equipos->num_rows()>0) {
        return $equipos->result();
      } else {
        return false;
      }
    }
    function obtenerPorId($id_equi){
        $this->db->where("id_equi",$id_equi);
        $equipo=$this->db->get("equipo");
        if($equipo->num_rows()>0){
            return $equipo->row();
        }
        return false;
    }
    public function eliminarPorId($id){
      $this->db->where("id_equi",$id);
      return $this->db->delete('equipo');
    }
    function actualizar($nombre_equi,$datos){
        $this->db->where("nombre_equi",$nombre_equi);
        return $this->db->update('equipo',$datos);
    }
}//cierre llave
