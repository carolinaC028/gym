<?php
  class Producto extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("producto",$datos);
    }
    public function obtenerTodo(){
      $productos=$this->db->get('producto');
      if ($productos->num_rows()>0) {
        return $productos->result();
      } else {
        return false;
      }
    }
  //   function borrar($id_pro)
  // {
  //   //delete from producto where id_ins = 1:
  //   $this->db->where("id_pro",$id_pro);
  //   if ($this->db->delete("producto")) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  //
  // }
    //FUNCION PARA CONSULTAR
  function obtenerPorId($id_pro){
    $this->db->where("id_pro", $id_pro);
    $producto=$this->db->get("producto");
    if ($producto->num_rows()>0) {
      return $producto->row();
    } else {
      return false;
    }
  }
  public function eliminarPorId($id){
    $this->db->where("id_pro",$id);
    return $this->db->delete('producto');
  }
    //FUNCION PARA ACTUALIZAR
    function actualizar($id_pro,$data){
      $this->db->where("id_pro",$id_pro);
      return $this->db->update('producto',$data);
    }
}//cierre de la clase
