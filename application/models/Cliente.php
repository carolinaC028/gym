<?php
  class Cliente extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("cliente",$datos);
    }
    public function obtenerTodo(){
      $clientes=$this->db->get('cliente');
      if ($clientes->num_rows()>0) {
        return $clientes->result();
      } else {
        return false;
      }
    }
    function obtenerPorId($id_clie){
        $this->db->where("id_clie",$id_clie);
        $cliente=$this->db->get("cliente");
        if($cliente->num_rows()>0){
            return $cliente->row();
        }
        return false;
    }
    public function eliminarPorId($id){
      $this->db->where("id_clie",$id);
      return $this->db->delete('cliente');
    }
    function actualizar($id_clie,$datos){
        $this->db->where("id_clie",$id_clie);
        return $this->db->update('cliente',$datos);
    }
}//cierre llave
