<?php
  class Entrenador extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("entrenador",$datos);
    }
    public function obtenerTodo(){
      $Entrenadores=$this->db->get('entrenador');
      if ($Entrenadores->num_rows()>0) {
        return $Entrenadores->result();
      } else {
        return false;
      }
    }
    function obtenerPorId($id_ent){
        $this->db->where("id_ent",$id_ent);
        $entrenador=$this->db->get("entrenador");
        if($entrenador->num_rows()>0){
            return $entrenador->row();
        }
        return false;
    }
    public function eliminarPorId($id){
      $this->db->where("id_ent",$id);
      return $this->db->delete('entrenador');
    }
    function actualizar($id_ent,$datos){
        $this->db->where("id_ent",$id_ent);
        return $this->db->update('entrenador',$datos);
    }
}//cierre llave
