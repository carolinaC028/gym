<?php
  class Limpieza extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("limpieza",$datos);
    }
    public function obtenerTodo(){
      $Limpiezas=$this->db->get('limpieza');
      if ($Limpiezas->num_rows()>0) {
        return $Limpiezas->result();
      } else {
        return false;
      }
    }
    function obtenerPorId($id_limp){
        $this->db->where("id_limp",$id_limp);
        $limpieza=$this->db->get("limpieza");
        if($limpieza->num_rows()>0){
            return $limpieza->row();
        }
        return false;
    }
    public function eliminarPorId($id){
      $this->db->where("id_limp",$id);
      return $this->db->delete('limpieza');
    }
    function actualizar($id_limp,$datos){
        $this->db->where("id_limp",$id_limp);
        return $this->db->update('limpieza',$datos);
    }
}//cierre llave
