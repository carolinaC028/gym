<?php
  class Rutina extends CI_Model
  {
    function __construct()
    {
    		parent::__construct();

    }

    public function insertar($datos){
      return $this->db->insert("rutina",$datos);
    }
    public function obtenerTodo(){
      $Rutinas=$this->db->get('rutina');
      if ($Rutinas->num_rows()>0) {
        return $Rutinas->result();
      } else {
        return false;
      }
    }
    function obtenerPorId($id_rut){
        $this->db->where("id_rut",$id_rut);
        $rutina=$this->db->get("rutina");
        if($rutina->num_rows()>0){
            return $rutina->row();
        }
        return false;
    }
    public function eliminarPorId($id){
      $this->db->where("id_rut",$id);
      return $this->db->delete('rutina');
    }
    function actualizar($id_rut,$datos){
        $this->db->where("id_rut",$id_rut);
        return $this->db->update('rutina',$datos);
    }
}//cierre llave
