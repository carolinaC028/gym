<?php if ($alimentaciones):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1><b><i>Listado de Plan de Alimentacion</i></b></h1>
            <tr>
                <th>ID</th>
                <th>DÍA</th>
                <th>TIPO</th>
                <th>COMIDA</th>
                <th>DETALLE</th>
                <th>CANTIDAD DE VASOS</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($alimentaciones as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_plan; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->dia_plan; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->tipo_plan; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->comida_plan; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->detalle_plan; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->vasos_plan; ?>
                    </td>
                  </tr>
            <?php endforeach; ?>
          </tbody>
