<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Listado de Plan de Alimentacion </p> </b></h1></div>
    </div>
    <center>
  <a href="<?php echo site_url('alimentaciones/nuevo') ?>"
<button type="button" name="button" class="btn btn-success">
<i class="mdi mdi-plus"></i> AGREGAR NUEVO
</a>
</center></br>
</button>
    <br>
    <br>
    <br>
  <?php if ($listadoAlimentaciones): ?>
  <table class="table table-striped table-bordered table-hover"id="tbl_alimentaciones">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">DÍA</th>
      <th class="text-center">TIPO</th>
      <th class="text-center">COMIDA</th>
      <th class="text-center">DETALLE</th>
      <th class="text-center">CANTIDAD VASOS</th>
      <th class="text-center">ACCIONES</th>

    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoAlimentaciones as $filaTemporal): ?>
      <tr>
        <td class="text-center"><?php echo  $filaTemporal->id_plan ?></td>
        <td class="text-center"><?php echo  $filaTemporal->dia_plan ?></td>
        <td class="text-center"><?php echo  $filaTemporal->tipo_plan ?></td>
        <td class="text-center"><?php echo $filaTemporal->comida_plan ?></td>
        <td class="text-center"><?php echo $filaTemporal->detalle_plan?></td>
        <td class="text-center"><?php echo $filaTemporal->vasos_plan ?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("alimentaciones/editar");?>/<?php echo $filaTemporal->id_plan ; ?>"class="btn btn-warning">
          <i class="mdi mdi-lead-pencil" ></i>Editar</a>

          <!-- <a  href="<?php echo site_url("alimentaciones/borrar");?>/<?php echo $filaTemporal->id_plan ; ?>"class="btn btn-danger"onclick="return confirm('¿Esta seguro que desea eliminar?');">
          <i class="mdi mdi-lead-circle" ></i>Eliminar</a> -->
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/alimentaciones/eliminar/<?php echo $filaTemporal->id_plan; ?>"class="btn btn-warning"
                            title="Eliminar Alimentacion"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>


        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <center><h3><b style="color:red">No existe Plan de Alimentación</b></h3></center>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_alimentaciones")
    .DataTable();
</script>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
