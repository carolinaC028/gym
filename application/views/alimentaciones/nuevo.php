<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">INGRESE UN NUEVO PLAN DE ALIMENTACION </p> </b></h1></div>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_alimentacion"action="<?php echo site_url("alimentaciones/guardarAlimentaciones"); ?>" method="post">
    <div class="container">
        <div class="row">
          <div class="col-md-4 text-center">
              <label for="">Ingrese el Día:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <select class"form-control" type="text" name="dia_plan"  class="form-control input-sm " required><option value="">----Elija el día---</option>
                  <option value"LUNES">LUNES</option>
                  <option value="MARTES">MARTES</option>
                  <option value"MIERCOLES">MIERCOLES</option>
                  <option value="JUEVES">JUEVES</option>
                  <option value"VIERNES">VIERNES</option>
                  <option value="SABADO">SABADO</option>
                  <option value"DOMINGO">DOMINGO</option>
              </select>
          </div>
          <div class="col-md-4 text-center">
              <label for="">Ingrese el tipo:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese el tipo" class="form-control" name="tipo_plan" value=""
              id="tipo_plan"required>
              <br>
          </div>
          <div class="col-md-4 text-center">
              <label for="">Ingrese la comida:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese la comida" class="form-control" name="comida_plan" value=""
              id="comida_plan"required>
              <br>
          </div>


        </div>
        <div class="row">
          <div class="col-md-6 text-center">
              <label for="">Ingrese el detalle:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese el detalle" class="form-control" name="detalle_plan" value=""
              id="detalle_plan"required>
              <br>
          </div>
          <div class="col-md-6 text-center">
              <label for="">Ingrese la cantidad de vasos de agua:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="number" placeholder="Ingrese la cantidad de vasos de agua" class="form-control" name="vasos_plan" value=""
              id="vasos_plan"required>
              <br>
          </div>
        </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <br>
                <br>
                <a href="<?php echo site_url(); ?>/alimentaciones/index"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
        <br>
</form>

<script type="text/javascript">
$("#frm_nuevo_alimentacion").validate({
rules:{
  dia_plan:{
    required:true,

  },
  tipo_plan:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  comida_plan:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  detalle_plan:{
    required:true,
    minlength:2,
    maxlength:250,
    letras:true
  },
  vasos_plan:{
    required:true,
    minlength:1,
    maxlength:5,
    digits:true
  },

},
messages:{
  dia_plan:{
    required:"Ingrese el dia por favor",
  
  },
  tipo_plan:{
    required:"Ingrese el tipo por favor",
    minlength:"Escriba mas de 3 caracteres",
    maxlength:"incorrecto",
    letras:"Este campo acepta solo letras"
  },
  comida_plan:{
    required:"Ingrese las comidas",
    minlength:"Ingrese al menos 1 digito correspondiente",
    maxlength:"Ingresado incorrecto",
    letras:"este campo solo acepta letras"
  },
  detalle_plan:{
    required:"Ingrese el detalle por favor",
    minlength:"Escriba mas de 2 caracteres",
    maxlength:"incorrecto",
    letras:"Este campo acepta solo letras"
  },
  vasos_plan:{
    required:"Ingrese la cantidad de vasos",
    minlength:"Ingrese al menos 1 digito correspondiente",
    maxlength:"Cantidad de Vasos incorrecto",
    digits:"Este campo solo acepta numeros",
    number:"Este campo solo acepta numeros"
  },

}
});
</script>
