<?php if ($this->session->flashdata("confirmacion")): ?>
  <script type="text/javascript">
	toastr.success("<?php echo $this->session->flashdata("confirmacion"); ?>");
  </script>
  <?php $this->session->set_flashdata("confirmacion",""); ?>
<?php endif; ?>

<?php if ($this->session->flashdata("bienvenida")): ?>
  <script type="text/javascript">
	toastr.success("<?php echo $this->session->flashdata("bienvenida"); ?>");
  </script>
  <?php $this->session->set_flashdata("bienvenida"," "); ?>
<?php endif; ?>

<?php if ($this->session->flashdata("error")): ?>
  <script type="text/javascript">
	toastr.error("<?php echo $this->session->flashdata("error"); ?>");
  </script>
  <?php $this->session->set_flashdata("error",""); ?>
<?php endif; ?>
<style media="screen">
  .obligatorio{
    color:red;
    background-color:white;
    border: radius 20px;
    font-size:10px;
    padding-left:6px;
    padding-right:6px;
  }
  .error{
    color:red;
    font-weight:bold;
  }
  input.error{
    border:2px solid red;
  }
</style>

  <!-- info section -->

  <section class="info_section layout_padding2-top">
    <div class="container">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <h6>
            Sobre nosotros
          </h6>
          <p>
            Ser el gimnasio líder de la ciudad y el país, brindando bienestar a nuestros miembros, generando valor a nuestra empresa, a nuestros colaboradores y a nuestra comunidad.
          </p>
        </div>
        <div class="col-md-2 offset-md-1">
          <h6>
            Menus
          </h6>
          <ul>
            <li class=" active">
              <a class="" href="index.html">INICIO <span class="sr-only">(current)</span></a>
            </li>
            <li class="">
              <a class="" href="about.html">SOBRE NOSOTROS </a>
            </li>
            <li class="">
              <a class="" href="service.html">SERVICIOS </a>
            </li>
            <li class="">
              <a class="" href="#contactSection">CONTACTANOS</a>
            </li>
          </ul>
        </div>
        <div class="col-md-3">
          <h6>
            Importancia
          </h6>
          <ul>
            <li>
              <a href="">
                DISIPLINA
              </a>
            </li>
            <li>
              <a href="">
                CAPACIDAD
              </a>
            </li>
            <li>
              <a href="">
                VALENTIA
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-3">
          <h6>
            Contactos
          </h6>
          <div class="info_link-box">
            <a href="">
              <img src="<?php echo base_url('plantilla/images/location-white.png');?>" alt="">
              <span> Latacunga, San Felipe</span>
            </a>
            <a href="">
              <img src="<?php echo base_url('plantilla/images/call-white.png');?>" alt="">
              <span>098721067</span>
            </a>
            <a href="">
              <img src="<?php echo base_url('plantilla/images/mail-white.png');?>" alt="">
              <span> zeusgym@gmail.com</span>
            </a>
          </div>
          <div class="info_social">
            <div>
              <a href="">
                <img src="<?php echo base_url('plantilla/images/facebook-logo-button.png');?>" alt="">
              </a>
            </div>
            <div>
              <a href="">
                <img src="<?php echo base_url('plantilla/images/twitter-logo-button.png');?>" alt="">
              </a>
            </div>
            <div>
              <a href="">
                <img src="<?php echo base_url('plantilla/images/linkedin.png');?>" alt="">
              </a>
            </div>
            <div>
              <a href="">
                <img src="<?php echo base_url('plantilla/images/instagram.png');?>" alt="">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end info section -->


  <!-- footer section -->
  <section class="container-fluid footer_section ">
    <p>
      &copy;DERECHOS RESERVADOS
      <a href="https://html.design/">Karo & Angelica & Jairo</a>
    </p>
  </section>
  <!-- footer section -->

  <script type="text/javascript" src="<?php echo  base_url('plantilla/js/jquery-3.4.1.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo  base_url('plantilla/js/bootstrap.js');?>"></script>

  <script>
    function openNav() {
      document.getElementById("myNav").classList.toggle("menu_width");
      document
        .querySelector(".custom_menu-btn")
        .classList.toggle("menu_btn-style");
    }
  </script>
</body>

</html>
