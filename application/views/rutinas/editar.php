<div class="row">
  <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">EDITAR RUTINAS</p> </b></h1></div>
  </div>
</div>
<form class=""
id="frm_editar_rutina"
action="<?php echo site_url('rutinas/procesarActualizaciones'); ?>"
method="post">
<div class="container">
    <div class="row">
      <input type="hidden" name="id_rut" id="id_rut" value="<?php echo $rutinaEditar->id_rut; ?>">
    </div>

    <div class="row">
      <div class="col-md-4 text-center">
          <label for="">tipo de rutina:
              <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text" placeholder="Ingrese tipo de rutina" class="form-control" name="tipo_rut" value="<?php echo $rutinaEditar->tipo_rut; ?>"
          id="tipo_rut" required>
          <br>
      </div>
      <div class="col-md-4 text-center">
          <label for="">Ingrese duracion de rutina:
              <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text" placeholder="Ingrese su nombre" class="form-control" name="duracion_rut" value="<?php echo $rutinaEditar->duracion_rut; ?>"
          id="duracion_rut"required>
          <br>
      </div>

      <div class="col-md-4 text-center">
          <label for="">Ingrese el Detalle de rutina:
              <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text" placeholder="Ingrese su apellido" class="form-control" name="detalle_rut" value="<?php echo $rutinaEditar->detalle_rut; ?>"
          id="detalle_rut"required>
          <br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center">
          <label for="">Ingrese el dia de rutina:
              <span class="obligatorio">(Obligatorio)</span>
          </label>
          <br>
          <input type="text" placeholder="Ingrese su dia de rutina" class="form-control" name="dia_rut" value="<?php echo $rutinaEditar->dia_rut; ?>"
          id="dia_rut"required>
          <br>
      </div>
    </div>
  </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <br>
            <br>
            <button type="submit" name="button" class="btn btn-primary">Actualizar</button>
            <a href="<?php echo site_url(); ?>/rutinas/listas"class="btn btn-danger">
            Cancelar</a>
        </div>
    </div>
    <br>
    </form>
    <br>
    <br>

<script type="text/javascript">
    $("#frm_nuevo_rutina").validate({
      rules:{
      tipo_rut:{
      required:true,
      minlength:3,
      maxlength:250,
      letras:true
      },
      duracion_rut:{
      required:true,
      minlength:3,
      maxlength:250,
      letras:true
      },
      detalle_rut:{
      required:true,
      minlength:2,
      maxlength:250,
      letras:true
      },
      dia_rut:{
      required:true,
      minlength:2,
      maxlength:250,
      letras:true
      },

      },
      messages:{
        tipo_rut:{
          required:"Ingrese el tipo por favor",
          minlength:"Escriba mas de 3 caracteres",
          maxlength:"Nombre incorrecto",
          letras:"Este campo acepta solo letras"
        },
        duracion_rut:{
          required:"Ingrese su duracion por favor",
          minlength:"Ingrese al menos 3 digito",
          maxlength:"Apellido incorrecto",
          letras:"Este campo solo acepta letras"
        },
        detalle_rut:{
          required:"Ingrese el detalle por favor",
          minlength:"Escriba mas de 3 caracteres",
          maxlength:"Direccion incorrecto",
          letras:"Este campo acepta solo letras"
        },
        dia_rut:{
          required:"Ingrese el dia por favor",
          minlength:"Escriba mas de 3 caracteres",
          maxlength:"Direccion incorrecto",
          letras:"Este campo acepta solo letras"
        },

    }
   });
  </script>
