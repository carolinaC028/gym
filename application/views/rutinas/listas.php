<?php if ($empleados):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1><b><i>Listado de rutinas</i></b></h1>
            <tr>
                <th>ID</th>
                <th>TIPO DE RUTINA</th>
                <th>DURACION DE RUTINA</th>
                <th>DETALLE DE RUTINA</th>
                <th>DIA DE RUTINA</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($equipos as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_rut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->tipo_rut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->duracion_rut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->detalle_rut; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->dia_rut; ?>
                    </td>

            </tr>
            <?php endforeach; ?>
          </tbody>
