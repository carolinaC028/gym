<br>
<br>
<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Listado de rutinas</p> </b></h1></div>
    </div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('rutinas/nuevo'); ?>" class="btn btn-success">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Rutina
       </a>
    </div>
    <br>
    <br>
    <br>
  <?php if ($listadoRutinas): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_rutinas">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">TIPO DE RUTINA</th>
      <th class="text-center">DURACION DE RUTINA</th>
      <th class="text-center">DETALLE DE RUTINA</th>
      <th class="text-center">DIA DE RUTINA</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoRutinas as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_rut ?></td>
        <td class="text-center"><?php echo $Temporal->tipo_rut?></td>
        <td class="text-center"><?php echo  $Temporal->duracion_rut ?></td>
        <td class="text-center"><?php echo $Temporal->detalle_rut ?></td>
        <td class="text-center"><?php echo $Temporal->dia_rut ?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("rutinas/editar");?>/<?php echo $Temporal->id_rut ; ?> "class="btn btn-danger"
          <i class="glyphicon glyphicon-trash" ></i>Editar</a>
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/rutinas/eliminar/<?php echo $Temporal->id_rut; ?>"class="btn btn-warning"
                            title="Eliminar Rutina"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>
           <!-- <a href="<?php echo site_url("proveedores/borrar") ?>/<?php echo $Temporal->id_rut ; ?>" class="btn btn-warning"
             title="Eliminar Rutina"
             onclick="return confirm('Esta seguro de eliminar Rutina');"></i>Eliminar</a> -->
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe rutinas</b></h3>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_rutinas")
    .DataTable();
</script>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
