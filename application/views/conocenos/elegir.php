<br>
<br>
<br>
<br>

<section class="us_section layout_padding">
  <div class="container">
    <div class="heading_container">
      <h2>
        POR QUE ELEGIRNOS?
      </h2>
    </div>
    <div class="us_container">
      <div class="box">
        <div class="img-box">
          <img src="<?php echo base_url('plantilla/images/u-1.png');?>" alt="">
        </div>
        <div class="detail-box">
          <h5>
          EQUIPOS DE CALIDAD
          </h5>
          <p>
            El gimnasio es el mejor lugar para hacerte más activo y afilado en la salud, eslogan perfecto para atraer personas.
          </p>
        </div>
      </div>
      <div class="box">
        <div class="img-box">
          <img src="<?php echo base_url('plantilla/images/u-2.png');?>" alt="">
        </div>
        <div class="detail-box">
          <h5>
            PLAN DE NUTRICIÓN SALUDABLE
          </h5>
          <p>
            Engordar al ajuste
          </p>
        </div>
      </div>
      <div class="box">
        <div class="img-box">
          <img src="<?php echo base_url('plantilla/images/u-3.png');?>" alt="">
        </div>
        <div class="detail-box">
          <h5>
            MAS SERVICIOS
          </h5>
          <p>
            Los mejores servicos que podemos brindar hacia nuestros clientes
          </p>
        </div>
      </div>
      <div class="box">
        <div class="img-box">
          <img src="<?php echo base_url('plantilla/images/u-4.png');?>" alt="">
        </div>
        <div class="detail-box">
          <h5>
            UNICA EN SUS NECESIDADES
          </h5>
          <p>
            Obtenemos los mejores resultados hacia nuestros clientes
          </p>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- end us section -->


<!-- client section -->

<section class="client_section layout_padding">
  <div class="container">
    <div class="heading_container">
      <h2>
        LO QUE NUESTROS CLIENTES MENCIONAN DE NOSOTROS
      </h2>
    </div>
    <div id="carouselExample2Indicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExample2Indicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExample2Indicators" data-slide-to="1"></li>
        <li data-target="#carouselExample2Indicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="box">
            <div class="img-box">
              <img src="<?php echo base_url('plantilla/images/client.png');?>" alt="">
            </div>
            <div class="detail-box">
              <h5>
                COMENTARIOS
              </h5>
              <p>
                Es raro, hoy en día, encontrar un gimnasio cuyos monitores estén pendientes de ti seas cliente habitual o solo de paso. Que te pongan tus tablas preguntándote y entendiendo tus problemas traumatologicos y que sepan recomendarte una dieta o aconsejarte sobre nutrición. Las instalaciones tienen variedad de maquinas y bancos para que no tengas que estar esperando para hacer tus ejercicios.
Este gimnasio lo tiene todo, están al día con las ultimas técnicas, tienen una variedad de ejercicios extensas si quieres hacer solo musculación y clases muy variadas incluidas clases para los niños.
Conclusión: cada vez que vaya a Vera o la zona de Mojacar ya tengo un gimnasio de referencia.
Por ponerle alguna pega: vivo a 5 horas en coche, si no , me haría cliente habitual.
              </p>
            </div>
          </div>
        </div>
  </div>
</section>
