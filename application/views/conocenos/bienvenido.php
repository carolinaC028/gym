<section class="about_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2>
          Sobre "ZEUS-GYM"
        </h2>
      </div>
      <div class="box">
        <div class="img-box">
          <img src="<?php echo base_url('plantilla//images/about-img.png');?>" alt="">
        </div>
        <div class="detail-box">
          <p>
            Brindar a nuestros miembros una salud física y mental, para ayudarles a alcanzar sus objetivos individuales; con nuestra amplia experiencia les proveemos bienestar en base a un esmerado servicio, a un ambiente agradable y con un personal entrenado en los últimos conocimientos disponibles.
          </p>
        </div>
      </div>
    </div>
  </section>
  <section class="contact_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2>
          <span>
            Estamos ubicados en:
          </span>
        </h2>
      </div>
      <div class="layout_padding2-top">
        <div class="row">
          <div class="col-md-6 ">
          </div>
          <div class="col-md-12">
            <div class="map_container">
              <div class="map-responsive">
                <iframe
                  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&q=Eiffel+Tower+Paris+France"
                  width="600" height="300" frameborder="0" style="border:0; width: 100%; height:100%"
                  allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end contact section -->
