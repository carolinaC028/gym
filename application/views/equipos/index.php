<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Listado Equipos</p> </b></h1></div>
    </div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-success">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Equipo
       </a>
    </div>
    <br>
    <br>
    <br>
  <?php if ($listadoEquipos): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_equipos">
<thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">ESTADO</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">MARCA</th>
      <th class="text-center">DESCRIPCION</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoEquipos as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_equi ?></td>
        <td class="text-center"><?php echo $Temporal->estado_equi?></td>
        <td class="text-center"><?php echo  $Temporal->nombre_equi ?></td>
        <td class="text-center"><?php echo $Temporal->marca_equi ?></td>
        <td class="text-center"><?php echo $Temporal->descripcion_equi ?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("equipos/editar");?>/<?php echo $Temporal->id_equi ; ?> "class="btn btn-danger"
          <i class="glyphicon glyphicon-trash" ></i>Editar</a>
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/equipos/eliminar/<?php echo $Temporal->id_equi; ?>"class="btn btn-warning"
                            title="Eliminar Equipo"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>
           <!-- <a href="<?php echo site_url("equipos/borrar") ?>/<?php echo $Temporal->id_equi ; ?>" class="btn btn-warning"
             title="Eliminar Equipo"
             onclick="return confirm('Esta seguro de eliminar equipo');"></i>Eliminar</a> -->
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe equipos</b></h3>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_equipos")
    .DataTable();
</script>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
