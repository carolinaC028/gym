<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">INGRESE UN NUEVO EQUIPO</p> </b></h1></div>
    </div>
<br>
<form class="" id="frm_nuevo_equipo"action="<?php echo site_url("equipos/guardarEquipos"); ?>" method="post">
<div class="container">
  <div class="row">
      <div class="col-md-4 text-right">
          <label for="">ESTADO:</label>
      </div>
      <div class="col-md-2">
        <select class"form-control" type="text" name="estado_equi"  class="form-control input-sm " required><option value="">----ESTADO---</option>
            <option value"ACTIVO">ACTIVO</option>
            <option value="INACTIVO">INACTIVO</option>
        </select>
      </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row">
      <!-- <div class="col-md-4">
        <label for="">Ingrese Estado Equipo:
            <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="text" placeholder="Ingrese su nombre" class="form-control" name="estado_equi" value=""
        id="estado_equi" required>
        <br>
    </div> -->
    <div class="col-md-4">
      <label for="">Ingrese Nombre Equipo:
          <span class="obligatorio">(Obligatorio)</span>
      </label>
      <br>
      <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre_equi" value=""
      id="nombre_equi" required>
      <br>
  </div>
    <div class="col-md-4">
        <label for="">Ingrese la Marca:
            <span class="obligatorio">(Obligatorio)</span>
        </label>
        <br>
        <input type="text" placeholder="Ingrese su marca" class="form-control" name="marca_equi" value=""
        id="marca_equi"required>
        <br>
    </div>
        <div class="col-md-4">
            <label for="">Ingrese su descripcion:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su descrpcion" class="form-control" name="descripcion_equi" value=""
            id="descripcion_equi"required>
            <br>
        </div>
        </div>
</div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <a href="<?php echo site_url(); ?>/equipos/listas"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
      </div>
</form>
<br>
<br>

<script type="text/javascript">
$("#frm_nuevo_equipo").validate({
rules:{
    nombre_equi:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  marca_equi:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  descripcion_equi:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
},
messages:{
  nombre_equi:{
    required:"Ingrese su nombre por favor",
    minlength:"Ingrese el nombre al menos de 3 digitos",
    maxlength:"Nombre incorrecto",
    letras:"Este campo acepta solo letras"
  },
  marca_equi:{
    required:"Ingrese su marca por favor",
    minlength:"Ingrese el marca al menos de 3",
    maxlength:"Marca incorrecto",
    letras:"Este campo acepta solo letras"
  },
  descripcion_equi:{
    required:"Ingrese su descrpcion por favor",
    minlength:"Ingrese su descripcion al menos de 5",
    maxlength:"Direccion incorrecto",
    letras:"Este campo acepta solo letras"
  },

}
});
</script>
