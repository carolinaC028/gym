<?php if ($empleados):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1><b><i>Listado de equipos</i></b></h1>
            <tr>
                <th>ID</th>
                <th>ESTADO</th>
                <th>NOMBRE</th>
                <th>MARCA</th>
                <th>DESCRPCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($equipos as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_equi; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_equi; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->estado_equi; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->marca_equi; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->descripcion_equi; ?>
                    </td>

            </tr>
            <?php endforeach; ?>
          </tbody>
