<?php if ($productos):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1><b><i>Listado de Productos</i></b></h1>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DETALLE</th>
                <th>PRECIO</th>
                <th>MARCA</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productos as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_pro; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_pro; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->detalle_pro; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->precio_pro; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->marca_pro; ?>
                    </td>
                  </tr>
            <?php endforeach; ?>
          </tbody>
