<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">INGRESE UN NUEVO PRODUCTO </p> </b></h1></div>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_producto"action="<?php echo site_url("productos/guardarProductos"); ?>" method="post">
    <div class="container">
        <div class="row">
          <div class="col-md-4 text-center">
              <label for="">Ingrese el Producto:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese el nombre del producto" class="form-control" name="nombre_pro" value=""
              id="nombre_pro" required>
              <br>
          </div>
          <div class="col-md-4 text-center">
              <label for="">Ingrese el detalle:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese el detalle" class="form-control" name="detalle_pro" value=""
              id="detalle_pro"required>
              <br>
          </div>

          <div class="col-md-4 text-center">
              <label for="">Ingrese el precio:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="number" placeholder="Ingrese el precio" class="form-control" name="precio_pro" value=""
              id="precio_pro"required>
              <br>
          </div>
        </div>
        <div class="col-md-12 text-center">
            <label for="">Ingrese la marca:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese la marca" class="form-control" name="marca_pro" value=""
            id="marca_pro"required>
            <br>
         </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <br>
                <br>
                <a href="<?php echo site_url(); ?>/productos/index"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
        <br>
</form>

<script type="text/javascript">
$("#frm_nuevo_producto").validate({
rules:{
  nombre_pro:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  detalle_pro:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  precio_pro:{
    required:true,
    minlength:1,
    maxlength:7,
    digits:true
  },
  marca_pro:{
    required:true,
    minlength:2,
    maxlength:250,
    letras:true
  },

},
messages:{
  nombre_pro:{
    required:"Ingrese el nombre por favor",
    minlength:"Ingrese el nombre al menos de 3 digitos",
    maxlength:"Nombre incorrecto",
    letras:"Este campo acepta solo letras"
  },
  detalle_pro:{
    required:"Ingrese el detalle por favor",
    minlength:"Escriba mas de 3 caracteres",
    maxlength:"incorrecto",
    letras:"Este campo acepta solo letras"
  },
  precio_pro:{
    required:"Ingrese el precio",
    minlength:"Ingrese al menos 1 digito correspondiente",
    maxlength:"Precio incorrecto",
    digits:"Este campo solo acepta numeros",
    number:"Este campo solo acepta numeros"
  },
  marca_pro:{
    required:"Ingrese la marca por favor",
    minlength:"Escriba mas de 2 caracteres",
    maxlength:"incorrecto",
    letras:"Este campo acepta solo letras"
  },

}
});
</script>
