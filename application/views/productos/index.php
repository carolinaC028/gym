<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Listado de Productos </p> </b></h1></div>
    </div>
    <center>
  <a href="<?php echo site_url('productos/nuevo') ?>"
<button type="button" name="button" class="btn btn-success">
<i class="glyphicon glyphicon-plus"></i> AGREGAR NUEVO
</a>
</center></br>
</button>
    <br>
    <br>
    <br>
  <?php if ($listadoProductos): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_productos">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">DETALLE</th>
      <th class="text-center">PRECIO</th>
      <th class="text-center">MARCA</th>
      <th class="text-center">ACCIONES</th>

    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoProductos as $filaTemporal): ?>
      <tr>
        <td class="text-center"><?php echo  $filaTemporal->id_pro ?></td>
        <td class="text-center"><?php echo  $filaTemporal->nombre_pro ?></td>
        <td class="text-center"><?php echo $filaTemporal->detalle_pro ?></td>
        <td class="text-center"><?php echo $filaTemporal->precio_pro?></td>
        <td class="text-center"><?php echo $filaTemporal->marca_pro ?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("productos/editar");?>/<?php echo $filaTemporal->id_pro ; ?>"class="btn btn-danger">
          <i class="mdi mdi-lead-pencil" ></i>Editar</a>

          <!-- <a  href="<?php echo site_url("productos/borrar");?>/<?php echo $filaTemporal->id_pro ; ?>"class="btn btn-danger"onclick="return confirm('¿Esta seguro que desea eliminar?');">
          <i class="mdi mdi-lead-circle" ></i>Eliminar</a> -->
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/productos/eliminar/<?php echo $filaTemporal->id_pro; ?>"class="btn btn-warning"
                            title="Eliminar producto"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>


        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <center><h3><b style="color:red">No existe Productos</b></h3></center>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_productos")
    .DataTable();
</script>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
