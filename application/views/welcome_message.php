<section class="about_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2>
          Sobre nosotros "Zeus-Gym"
        </h2>
      </div>
      <div class="box">
        <div class="img-box">
          <img src="<?php echo base_url('plantilla/images/about-img.png');?>" alt="">
        </div>
        <div class="detail-box">
          <p>
            Somos uno de los mejores gimnacios del Ecuador por tal motivo tenemos mejores resultados con nuestros clientes
          </p>
          <a href="">
            Leer más
          </a>
        </div>
      </div>
    </div>
  </section>
