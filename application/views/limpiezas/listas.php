<?php if ($empleados):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1><b><i>Listado de personal de limpieza</i></b></h1>
            <tr>
                <th>ID</th>
                <th>HORARIO</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($limpiezas as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_limp; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->horario_limp;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_limp; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->apellido_limp; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_limp; ?>
                    </td>

            </tr>
            <?php endforeach; ?>
          </tbody>
