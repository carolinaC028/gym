<div class="row">
    <div class="col-md-12 text-center">
      <h1><b><p style="color:#001f36;">Listado de personal de limpieza</p> </b></h1></div>
</div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('limpiezas/nuevo'); ?>" class="btn btn-success">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Nuevo Personal
       </a>
    </div>
    <br>
    <br>
  <?php if ($listadoPersonales): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_limpiezas">
<thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">HORARIO</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">APELLIDO</th>
      <th class="text-center">TELEFONO</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoPersonales as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_limp ?></td>
        <td class="text-center"><?php echo $Temporal->horario_limp ?></td>
        <td class="text-center"><?php echo  $Temporal->nombre_limp ?></td>
        <td class="text-center"><?php echo $Temporal->apellido_limp ?></td>
        <td class="text-center"><?php echo $Temporal->telefono_limp ?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("limpiezas/editar");?>/<?php echo $Temporal->id_limp ; ?> "class="btn btn-danger"
          <i class="glyphicon glyphicon-trash" ></i>Editar</a>
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/limpiezas/eliminar/<?php echo $Temporal->id_limp; ?>"class="btn btn-warning"
                            title="Eliminar limpieza"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>
           <!-- <a href="<?php echo site_url("limpiezas/borrar") ?>/<?php echo $Temporal->id_limp ; ?>" class="btn btn-warning"
             title="Eliminar personal"
             onclick="return confirm('Esta seguro de eliminar personal de limpieza');"></i>Eliminar</a> -->
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe personal de limpieza</b></h3>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_limpiezas")
    .DataTable();
</script>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
