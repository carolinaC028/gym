<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">INGRESE UN NUEVO PERSONAL DE LIMPIEZA </p> </b></h1></div>
</div>
<br>
<form class="" id="frm_nuevo_limpieza"action="<?php echo site_url("limpiezas/guardarPersonal"); ?>" method="post">
  <div class="row">
      <div class="col-md-4 text-right">
          <label for="">HORARIO:</label>
      </div>
      <div class="col-md-2">
        <select class"form-control" type="text" name="horario_limp"  class="form-control input-sm " required><option value="">----HORARIO---</option>
            <option value"LUNES">LUNES</option>
            <option value="MARTES">MARTES</option>
            <option value"MIERCOLES">MIERCOLES</option>
            <option value="JUEVES">JUEVES</option>
            <option value"VIERNES">VIERNES</option>
            <option value="SABADO">SABADO</option>
            <option value"DOMINGO">DOMINGO</option>
        </select>
      </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
<div class="container">
    <div class="row">
        <!-- <div class="col-md-4">
            <label for="">Ingrese Horario:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su horario" class="form-control" name="horario_limp" value=""
            id="horario_limp" required>
            <br>
        </div> -->
        <div class="col-md-4">
            <label for="">Ingrese Nombre:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre_limp" value=""
            id="nombre_limp" required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese su apellido:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su apellido" class="form-control" name="apellido_limp" value=""
            id="apellido_limp"required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese su telefono:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="number" placeholder="Ingrese su telefono" class="form-control" name="telefono_limp" value=""
            id="telefono_limp"required>
            <br>
        </div>
      </div>
</div>

        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <a href="<?php echo site_url(); ?>/limpiezas/listas"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
</form>
<br>
<br>

<script type="text/javascript">
$("#frm_nuevo_limpieza").validate({
rules:{
  nombre_ent:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  apellido_ent:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  telefono_ent:{
    required:true,
    minlength:10,
    maxlength:10,
    digits:true
  },


},
messages:{
  nombre_ent:{
    required:"Ingrese su nombre por favor",
    minlength:"Ingrese el nombre al menos de 3 digitos",
    maxlength:"Nombre incorrecto",
    letras:"Este campo acepta solo letras"
  },
  apellido_ent:{
    required:"Ingrese su apellido por favor",
    minlength:"Ingrese el apellido al menos de 3",
    maxlength:"Titulo incorrecto",
    letras:"Este campo acepta solo letras"
  },
  telefono_ent:{
    required:"Ingrese su telefono por favor",
    minlength:"Ingrese su telefono, al menos 10 digitos correspondientes",
    maxlength:"Telefono incorrecto",
    digits:"Este campo solo acepta numeros",
    number:"Este campo solo acepta numeros"
  },


}
});
</script>
