<!-- PARALOS ERRORES -->
<?php if (!$this->session->
userdata("conectado")): ?>
  <script type="text/javascript">
      window.location.href=
      "<?php echo site_url('welcome/login'); ?>";
  </script>
<?php endif; ?>
<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>ZEUS-GYM</title>

  <!-- slider stylesheet -->
  <link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css" />

  <!-- bootstrap core css -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('plantilla/css/bootstrap.css');?>">

  <!-- fonts style -->
  <link href="https://fonts.googleapis.com/css?family=Baloo+Chettan|Dosis:400,600,700|Poppins:400,600,700&display=swap"
    rel="stylesheet" />
  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="<?php echo base_url('plantilla/css/style.css');?>">
  <!-- responsive style -->
  <link rel="stylesheet" href="<?php echo base_url('plantilla/css/responsive.css');?>">
  <!-- importacion de jquery -->
    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
    <!-- importacion de jquery validate para validacion-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
     <!-- validar solo los campos en letras -->
 <script type="text/javascript">
    jQuery.validator.addMethod("letras", function(value, element) {
        //return this.optional(element) || /^[a-z]+$/i.test(value);
        return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáéíñóú ]*$/.test(value);

		}, "Este campo solo acepta letras");
 </script>
 <!-- ////importar toast -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
 <!-- importacion de datatables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
</head>

<body>
  <div class="hero_area">
    <!-- header section strats -->
    <header class="header_section">
      <div class="container">
        <nav class="navbar navbar-expand-lg custom_nav-container">
          <div class="contact_nav" id="">
            <ul class="navbar-nav ">
              <li class="nav-item menu-items">
                  <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <span class="menu-icon">
                      <i class="mdi mdi-laptop"></i>
                    </span>
                    <span class="menu-title"><b>Entrenadores</b></span>
                    <i class="menu-arrow"></i>
                  </a>
                  <br>
                  <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                      <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('entrenadores/nuevo'); ?>">Nuevo</a></li>
                      <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('entrenadores/index'); ?>">Listado</a></li>
                    </ul>
                  </div>
                </li>
              <li class="nav-item">
                <li class="nav-item menu-items">
                    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                      <span class="menu-icon">
                        <i class="mdi mdi-laptop"></i>
                      </span>
                      <span class="menu-title"><b>Clientes</b></span>
                      <i class="menu-arrow"></i>
                    </a>
                    <br>
                    <div class="collapse" id="ui-basic">
                      <ul class="nav flex-column sub-menu">
                        <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('clientes/nuevo'); ?>">Nuevo</a></li>
                        <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('clientes/index'); ?>">Listado</a></li>
                      </ul>
                    </div>
                  </li>
              </li>
              <li class="nav-item">
                <li class="nav-item menu-items">
                  <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <span class="menu-icon">
                      <i class="mdi mdi-laptop"></i>
                    </span>
                    <span class="menu-title"><b>Produtos</b></span>
                    <i class="menu-arrow"></i>
                  </a>
                  <br>
                  <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                      <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('productos/nuevo'); ?>">Nuevos</a></li>
                      <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('productos/index'); ?>">Listado</a></li>
                    </ul>
                  </div>
                </li>
              </li>
              <li class="nav-item">
                <li class="nav-item menu-items">
                  <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <span class="menu-icon">
                      <i class="mdi mdi-laptop"></i>
                    </span>
                    <span class="menu-title"><b>Equipos</b></span>
                    <i class="menu-arrow"></i>
                  </a>
                  <br>
                  <div class="collapse" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                      <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('equipos/nuevo'); ?>">Nuevos</a></li>
                      <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('equipos/index'); ?>">Listado</a></li>
                    </ul>
                  </div>
                </li>
              </li>
              <div class="contact_nav" id="">
                <ul class="navbar-nav ">
                  <li class="nav-item menu-items">
                      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                        <span class="menu-icon">
                          <i class="mdi mdi-laptop"></i>
                        </span>
                        <span class="menu-title"><b>Proveedor</b></span>
                        <i class="menu-arrow"></i>
                      </a>
                      <br>
                      <div class="collapse" id="ui-basic">
                        <ul class="nav flex-column sub-menu">
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('proveedores/nuevo'); ?>">Nuevo</a></li>
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('proveedores/index'); ?>">Listado</a></li>
                        </ul>
                      </div>
                    </li>
                  <li class="nav-item">
                    <li class="nav-item menu-items">
                        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                          <span class="menu-icon">
                            <i class="mdi mdi-laptop"></i>
                          </span>
                          <span class="menu-title"><b>Alimentacion</b></span>
                          <i class="menu-arrow"></i>
                        </a>
                        <br>
                        <div class="collapse" id="ui-basic">
                          <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('alimentaciones/nuevo'); ?>">Nuevo</a></li>
                            <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('alimentaciones/index'); ?>">Listado</a></li>
                          </ul>
                        </div>
                      </li>
                  </li>
                  <li class="nav-item">
                    <li class="nav-item menu-items">
                      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                        <span class="menu-icon">
                          <i class="mdi mdi-laptop"></i>
                        </span>
                        <span class="menu-title"><b>Rutina</b></span>
                        <i class="menu-arrow"></i>
                      </a>
                      <br>
                      <div class="collapse" id="ui-basic">
                        <ul class="nav flex-column sub-menu">
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('rutinas/nuevo'); ?>">Nuevos</a></li>
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('rutinas/index'); ?>">Listado</a></li>
                        </ul>
                      </div>
                    </li>
                  </li>
                  <li class="nav-item">
                    <li class="nav-item menu-items">
                      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                        <span class="menu-icon">
                          <i class="mdi mdi-laptop"></i>
                        </span>
                        <span class="menu-title"><b>Horarios</b></span>
                        <i class="menu-arrow"></i>
                      </a>
                      <br>
                      <div class="collapse" id="ui-basic">
                        <ul class="nav flex-column sub-menu">
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('horarios/nuevo'); ?>">Nuevos</a></li>
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('horarios/index'); ?>">Listado</a></li>
                        </ul>
                      </div>
                    </li>
                  </li>
                  <li class="nav-item">
                    <li class="nav-item menu-items">
                      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                        <span class="menu-icon">
                          <i class="mdi mdi-laptop"></i>
                        </span>
                        <span class="menu-title"><b>Personal</b></span>
                        <i class="menu-arrow"></i>
                      </a>
                      <br>
                      <div class="collapse" id="ui-basic">
                        <ul class="nav flex-column sub-menu">
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('limpiezas/nuevo'); ?>">Nuevos</a></li>
                          <li class="nav-item"> <a class="nav-link" href="<?php echo site_url('limpiezas/index'); ?>">Listado</a></li>
                        </ul>
                      </div>
                    </li>
                  </li>
            </ul>
          </div>
        </nav>
      </div>

    </header>
    <!-- end header section -->
    <!-- slider section -->
    <section class=" slider_section position-relative">
      <div class="container">
        <div class="custom_nav2">
          <nav class="navbar navbar-expand-lg custom_nav-container ">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <br>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <div class="d-flex  flex-column flex-lg-row align-items-center">
                <ul class="navbar-nav">

                  <li class="nav-item dropdown">
                <a class="nav-link" id="profileDropdown" href="#" data-toggle="dropdown">
                  <div class="navbar-profile">
                    <?php echo $this->session->userdata("conectado")->email_usu; ?>
                    <br>
                    <br>
                    <div class="preview-item-content">
                    <p class="preview-subject mb-1"
                        onclick="cerrarSistema();"><b>Salir</b></p>
                        <script type="text/javascript">
                            function cerrarSistema(){
                              window.location.href=
                              "<?php echo
                              site_url('welcome/cerrarSesion'); ?>";
                            }
                        </script>
                    </div>
                    <p class="mb-0 d-none d-sm-block navbar-profile-name">
                    </p>
                    <i class="mdi mdi-menu-down d-none d-sm-block"></i>
                  </div>
                </a>
              </li>
<br>
<br>
<br>
                  <li class="nav-item active">
                    <a class="nav-link" href="<?php echo site_url("conocenos/bienvenido"); ?>"><b>Inicio</b> <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php echo site_url("conocenos/elegir"); ?>"><b>Elegirnos</b></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php echo site_url("conocenos/servicio"); ?>"><b>Servicios</b></a>
                  </li>
            </div>
          </nav>
        </div>
      </div>
      <div class="slider_container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 col-md-7 offset-md-6 offset-md-5">
                    <div class="detail-box">
                      <h2>
                        La salud es vida!
                      </h2>
                      <h1>
                        NO TE RINDAS!
                      </h1>
                      <p>
                        El dolor que sientes hoy es la fuerza que sentirás mañana.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item ">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 col-md-7 offset-md-6 offset-md-5">
                    <div class="detail-box">
                      <h2>
                      La salud es vida!
                      </h2>
                      <h1>
                      NO TE RINDAS!
                      </h1>
                      <p>
                        La vida comienza al final de tu zona de confort.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item ">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 col-md-7 offset-md-6 offset-md-5">
                    <div class="detail-box">
                      <h2>
                        La salud es vida!
                      </h2>
                      <h1>
                        NO TE RINDAS!
                      </h1>
                      <p>
                        Los obstáculos no tienen que frenarte. Si te encuentras con una pared, no das la vuelta y abandonas. Encuentras la manera de subir a ella, pasar a través de ella o rodearla.
                      </p>
                      <div class="btn-box">
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- end slider section -->
  </div>
  <br>
  <br>
  <br>
