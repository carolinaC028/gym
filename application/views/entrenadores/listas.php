<?php if ($empleados):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1><b><i>Listado de entrenadores</i></b></h1>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>CORREO ELECTRONICO</th>
                <th>TELEFONO</th>
                <th>DIRECCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($entrenadores as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_ent; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_ent; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->apellido_ent; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->correo_ent; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_ent; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->direccion_ent;?>
                    </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
