<div class="row">
    <div class="col-md-12 text-center">
      <h1><b><p style="color:#001f36;">Listado de entrenadores </p> </b></h1></div>
</div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('entrenadores/nuevo'); ?>" class="btn btn-success">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Entrenador
       </a>
    </div>
    <br>
    <br>
  <?php if ($listadoEntrenadores): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_entrenadores">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">APELLIDO</th>
      <th class="text-center">CORREO ELECTRONICO</th>
      <th class="text-center">TELEFONO</th>
      <th class="text-center">DIRECCION</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoEntrenadores as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_ent ?></td>
        <td class="text-center"><?php echo  $Temporal->nombre_ent ?></td>
        <td class="text-center"><?php echo $Temporal->apellido_ent ?></td>
        <td class="text-center"><?php echo $Temporal->correo_ent?></td>
        <td class="text-center"><?php echo $Temporal->telefono_ent ?></td>
        <td class="text-center"><?php echo $Temporal->direccion_ent ?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("entrenadores/editar");?>/<?php echo $Temporal->id_ent ; ?> "class="btn btn-danger"
          <i class="glyphicon glyphicon-trash" ></i>Editar</a>
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/entrenadores/eliminar/<?php echo $Temporal->id_ent; ?>"class="btn btn-warning"
                            title="Eliminar Entrenador"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>
           <!-- <a href="<?php echo site_url("entrenadores/borrar") ?>/<?php echo $Temporal->id_ent ; ?>" class="btn btn-warning"
             title="Eliminar Entrenador"
             onclick="return confirm('Esta seguro de eliminar entrenador');"></i>Eliminar</a> -->
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe clientes</b></h3>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_entrenadores")
    .DataTable();
</script>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
