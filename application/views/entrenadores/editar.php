<div class="row">
  <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">EDITAR EQUIPO</p> </b></h1></div>
  </div>
</div>
<form class=""
id="frm_editar_entrenador"
action="<?php echo site_url('entrenadores/procesarActualizaciones'); ?>"
method="post">
<div class="container">
    <div class="row">
      <input type="hidden" name="id_ent" id="id_ent" value="<?php echo $entrenadorEditar->id_ent; ?>">
    </div>
    <div class="row">
        <div class="col-md-4">
            <label for="">Ingrese Nombre:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre_ent" value="<?php echo $entrenadorEditar->nombre_ent ?>"
            id="nombre_ent" required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese su apellido:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su apellido" class="form-control" name="apellido_ent" value="<?php echo $entrenadorEditar->apellido_ent ?>"
            id="apellido_ent"required>
            <br>
        </div>
        <div class="col-md-4">
            <label for="">Ingrese su correo:
                <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su correo" class="form-control" name="correo_ent" value="<?php echo $entrenadorEditar->correo_ent ?>"
            id="correo_ent"required>
            <br>
        </div>
      </div>
        <div class="row">
          <div class="col-md-6 text-center">
              <label for="">Ingrese su telefono:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="number" placeholder="Ingrese su telefono" class="form-control" name="telefono_ent" value="<?php echo $entrenadorEditar->telefono_ent ?>"
              id="telefono_ent"required>
              <br>
          </div>
          <div class="col-md-6">
              <label for="">Ingrese su direccion:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese su direccion" class="form-control" name="direccion_ent" value="<?php echo $entrenadorEditar->direccion_ent ?>"
              id="direccion_ent"required>
              <br>
          </div>
        </div>
</div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <button type="submit" name="button" class="btn btn-primary">Actualizar</button>
                <a href="<?php echo site_url(); ?>/entrenadores/index"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
</form>
<br>
<br>

<script type="text/javascript">
$("#frm_nuevo_entrenador").validate({
rules:{
  nombre_ent:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  apellido_ent:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  telefono_ent:{
    required:true,
    minlength:10,
    maxlength:10,
    digits:true
  },
  direccion_ent:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },

},
messages:{
  nombre_ent:{
    required:"Ingrese su nombre por favor",
    minlength:"Ingrese el nombre al menos de 3 digitos",
    maxlength:"Nombre incorrecto",
    letras:"Este campo acepta solo letras"
  },
  apellido_ent:{
    required:"Ingrese su apellido por favor",
    minlength:"Ingrese el apellido al menos de 3",
    maxlength:"Titulo incorrecto",
    letras:"Este campo acepta solo letras"
  },
  telefono_ent:{
    required:"Ingrese su telefono por favor",
    minlength:"Ingrese su telefono, al menos 10 digitos correspondientes",
    maxlength:"Telefono incorrecto",
    digits:"Este campo solo acepta numeros",
    number:"Este campo solo acepta numeros"
  },
  direccion_ent:{
    required:"Ingrese su direccion por favor",
    minlength:"Ingrese su direccion correcto al menos de 5",
    maxlength:"Direccion incorrecto",
    letras:"Este campo acepta solo letras"
  },


}
});
</script>
