<br>
<?php if ($clientes):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1>Listado de clientes</h1>
            <tr>
                <th>ID</th>
                <th>CEDULA</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>TELEFONO</th>
                <th>CORREO ELECTRONICO</th>
                <th>DIRECCION</th>
                <th>ENTRENADOR</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($clientes as $filaTemporal): ?>
            <tr>
                    <td>
                        <?php echo $filaTemporal->id_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->cedula_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->apellido_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->telefono_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->correo_clie; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->direccion_clie;?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->entrenador_id_ent; ?>
                    </td>
            </tr>
            <?php endforeach; ?>

        </tbody>

    </table>

    <?php else: ?>
        <h1>no hay clientes</h1>
        <?php endif;?>