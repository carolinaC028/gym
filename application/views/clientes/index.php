<div class="row">
    <div class="col-md-12 text-center">
      <h1><b><p style="color:#001f36;">LISTA DE CLIENTES </p> </b></h1></div>
</div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('clientes/nuevo'); ?>" class="btn btn-success">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Cliente
       </a>
    </div>
    <br>
    <br>
    <br>
  <?php if ($listadoClientes): ?>
  <table class="table table-striped table-bordered table-hover" id="tbl_clientes">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">CEDULA</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">APELLIDO</th>
      <th class="text-center">TELEFONO</th>
      <th class="text-center">CORREO ELECTRONICO</th>
      <th class="text-center">DIRECCION</th>
      <th class="text-center">ENTRENADOR</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoClientes as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_clie ?></td>
        <td class="text-center"><?php echo  $Temporal->cedula_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->nombre_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->apellido_clie?></td>
        <td class="text-center"> <?php echo $Temporal->telefono_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->correo_clie ?></td>
        <td class="text-center"> <?php echo $Temporal->direccion_clie?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("clientes/editar");?>/<?php echo $Temporal->id_clie ; ?>" class="btn btn-danger">
          <i class="glyphicon glyphicon-trash" ></i>Editar</a>
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/clientes/eliminar/<?php echo $Temporal->id_clie; ?>"class="btn btn-warning"
                            title="Eliminar Cliente"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>
                            Eliminar
                            </a>
                  <?php endif; ?>
           <!-- <a href="<?php echo site_url("clientes/borrar") ?>/<?php echo $Temporal->id_clie ; ?>" class="btn btn-warning"
             title="Eliminar Cliente"
             onclick="return confirm('Esta seguro de eliminar cliente');"></i>Eliminar</a> -->
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe clientes</b></h3>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_clientes")
    .DataTable();
</script>
  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
