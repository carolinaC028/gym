<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Ingrese nuevo CLIENTE </p> </b></h1></div>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_cliente" action="<?php echo site_url("clientes/guardarClientes"); ?>" method="post">
    <div class="container">
        <div class="col-md-12 text-center">
            <label for="">Ingrese su Cedula
                <span class="obligatorio">(Obligatorio)</span></label>
            </label>
            <br>
            <input type="number" placeholder="Ingrese su numero de cedula" class="form-control" name="cedula_clie" value=""
            id="cedula_clie" required>
            <br>
        </div>
        <div class="col-md-12 text-center">
            <label for="">Ingrese Nombre
            <span class="obligatorio">(Obligatorio)</span></label>
            <br>
            <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre_clie" value=""
            id="nombre_clie" required>
            <br>
        </div>
        <div class="col-md-12 text-center">
            <label for="">Ingrese su apellido
            <span class="obligatorio">(Obligatorio)</span></label>
            <br>
            <input type="text" placeholder="Ingrese su apellido" class="form-control" name="apellido_clie" value=""
            id="apellido_clie"required>
            <br>
        </div>

        <div class="col-md-12 text-center">
            <label for="">Ingrese su telefono
              <span class="obligatorio">(Obligatorio)</span></label>
            <br>
            <input type="number" placeholder="Ingrese su numero de telefono" class="form-control" name="telefono_clie" value=""
            id="telefono_clie"required>
            <br>
        </div>
        <div class="col-md-12 text-center">
            <label for="">Ingrese su correo electrónico
              <span class="obligatorio">(Obligatorio)</span>
            </label>
            <br>
            <input type="text" placeholder="Ingrese su numero de telefono" class="form-control" name="correo_clie" value=""
            id="correo_cli"required>
            <br>
        </div>
        </div>
        <div class="container">
          <div class="col-md-12 text-center">
            <label for="">Ingrese su direccion
            <span class="obligatorio">(Obligatorio)</span>
          </label>
            <br>
            <input type="text" placeholder="Ingrese su direccion" class="form-control" name="direccion_clie" value=""
            id="direccion_clie"require>
            <br>
        </div>
          </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <br>
                <br>
                <a href="<?php echo site_url(); ?>/clientes/listas"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
        <br>
        <br>
</form>
        <script type="text/javascript">
          $("#frm_nuevo_cliente").validate({
            rules:{
              nombre_clie:{
                required:true,
                minlength:3,
                maxlength:250,
                letras:true
              },
              apellido_clie:{
                required:true,
                minlength:3,
                maxlength:250,
                letras:true
              },
              telefono_clie:{
                required:true,
                minlength:10,
                maxlength:10,
                digits:true
              },
              direccion_clie:{
                required:true,
                minlength:3,
                maxlength:250,
                letras:true
              },
              cedula_clie:{
                required:true,
                minlength:10,
                maxlength:10,
                digits:true
              },

            },
            messages:{
              cedula_clie:{
                required:"Ingrese su numero de cedula",
                minlength:"Ingrese el numero de cedula al menos de 10 digitos",
                maxlength:"cedula incorrecta",
                digits:"Este campo solo acepta numeros",
                number:"Este campo solo acepta numeros"
              },
              nombre_clie:{
                required:"Ingrese su nombre por favor",
                minlength:"Ingrese el nombre al menos de 3 digitos",
                maxlength:"Nombre incorrecto",
                letras:"Este campo acepta solo letras"
              },
              apellido_clie:{
                required:"Ingrese su apellido por favor",
                minlength:"Ingrese el apellido al menos de 3",
                maxlength:"Titulo incorrecto",
                letras:"Este campo acepta solo letras"
              },
              telefono_clie:{
                required:"Ingrese su telefono por favor",
                minlength:"Ingrese su telefono, al menos 10 digitos correspondientes",
                maxlength:"Telefono incorrecto",
                digits:"Este campo solo acepta numeros",
                number:"Este campo solo acepta numeros"
              },
              direccion_clie:{
                required:"Ingrese su direccion por favor",
                minlength:"Ingrese su direccion correcto al menos de 5",
                maxlength:"Direccion incorrecto",
                letras:"Este campo acepta solo letras"
              },
              correo_clie:{
                required:"Ingrese su direccion por favor",
                minlength:"Ingrese su direccion correcto al menos de 25",
                maxlength:"Direccion email incorrecto",
                letras:"Este campo acepta solo letras y numeros"
              },


            }
          });
        </script>
