<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Listado de Horarios </p> </b></h1></div>
    </div>
    <center>
  <a href="<?php echo site_url('horarios/nuevo') ?>"
<button type="button" name="button" class="btn btn-success">
<i class="mdi mdi-plus"></i> AGREGAR NUEVO
</a>
</center></br>
</button>
    <br>
    <br>
    <br>
  <?php if ($listadoHorarios): ?>
<table class="table table-striped table-bordered table-hover" id="tbl_horarios">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">DÍA</th>
      <th class="text-center">HORA</th>
      <th class="text-center">TIPO DE EJERCICIO</th>
      <th class="text-center">ACCIONES</th>

    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoHorarios as $filaTemporal): ?>
      <tr>
        <td class="text-center"><?php echo  $filaTemporal->id_hor ?></td>
        <td class="text-center"><?php echo  $filaTemporal->dia_hor ?></td>
        <td class="text-center"><?php echo $filaTemporal->hora_hor ?></td>
        <td class="text-center"><?php echo $filaTemporal->tipo_hor?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("horarios/editar");?>/<?php echo $filaTemporal->id_hor ; ?>"class="btn btn-warning">
          <i class="mdi mdi-lead-pencil" ></i>Editar</a>

          <!-- <a  href="<?php echo site_url("horarios/borrar");?>/<?php echo $filaTemporal->id_hor ; ?>"class="btn btn-danger"onclick="return confirm('¿Esta seguro que desea eliminar?');">
          <i class="mdi mdi-lead-circle" ></i>Eliminar</a> -->
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/horarios/eliminar/<?php echo $filaTemporal->id_hor; ?>"class="btn btn-warning"
                            title="Eliminar Horario"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>


        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <center><h3><b style="color:red">No existe Horarios</b></h3></center>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_horarios")
    .DataTable();
</script>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
