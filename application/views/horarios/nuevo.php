<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">INGRESE UN NUEVO HORARIO </p> </b></h1></div>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_horario"action="<?php echo site_url("horarios/guardarHorarios"); ?>" method="post">
    <div class="container">
        <div class="row">
          <div class="col-md-4 text-center">
              <label for="">Ingrese el Día:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>

                <select class"form-control" type="text" name="dia_hor"  class="form-control input-sm " required><option value="">----Elija el día---</option>
                    <option value"LUNES">LUNES</option>
                    <option value="MARTES">MARTES</option>
                    <option value"MIERCOLES">MIERCOLES</option>
                    <option value="JUEVES">JUEVES</option>
                    <option value"VIERNES">VIERNES</option>
                    <option value="SABADO">SABADO</option>
                    <option value"DOMINGO">DOMINGO</option>
                </select>

          </div>
          <div class="col-md-4 text-center">
              <label for="">Ingrese la hora:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese la hora" class="form-control" name="hora_hor" value=""
              id="hora_hor"required>
              <br>
          </div>
         </div>

                   <div class="col-md-6 text-center">
                       <label for="">Ingrese el tipo de ejercicio:
                           <span class="obligatorio">(Obligatorio)</span>
                       </label>
                       <br>
                       <input type="text" placeholder="Ingrese el tipo de ejercicio" class="form-control" name="tipo_hor" value=""
                       id="tipo_hor"required>
                       <br>
                   </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <br>
                <br>
                <a href="<?php echo site_url(); ?>/horarios/index"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
        <br>
</form>

<script type="text/javascript">
$("#frm_nuevo_horario").validate({
rules:{
  dia_hor:{
    required:true,

  },
  hora_hor:{
    required:true,
    minlength:2,
    maxlength:5,

  },
  tipo_hor:{
    required:true,
    minlength:2,
    maxlength:50,
    letras:true
  },

},
messages:{
  dia_hor:{
    required:"Ingrese el día por favor",
  

  },
  hora_hor:{
    required:"Ingrese la hora por favor",
    minlength:"Escriba mas de 3 caracteres",
    maxlength:"incorrecto",

  },
  tipo_hor:{
    required:"Ingrese el tipo de ejercicio por favor",
    minlength:"Escriba mas de 2 caracteres",
    maxlength:"incorrecto",
    letras:"Este campo acepta solo letras"
  },

}
});
</script>
