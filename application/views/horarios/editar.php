<div class="row">
  <div class="col-md-12 text-center well">
      <h3>EDITAR HORARIOS</h3>
  </div>
</div>
<br>
<form class="" id="frm_editar_horario" action="<?php echo site_url('horarios/procesarActualizacion'); ?>" method="post">
<div class="row">
  <input type="hidden" name="id_hor" id="id_hor" value="<?php echo $horarioEditar->id_hor; ?>">

  <div class="col-md-4 text-center">
      <label for="">Ingrese el Día:
          <span class="obligatorio">(Obligatorio)</span>
      </label>
      <select class"form-control" type="text" name="dia_hor"  class="form-control input-sm " required><option value="">----Elija el día---</option>
          <option value"LUNES">LUNES</option>
          <option value="MARTES">MARTES</option>
          <option value"MIERCOLES">MIERCOLES</option>
          <option value="JUEVES">JUEVES</option>
          <option value"VIERNES">VIERNES</option>
          <option value="SABADO">SABADO</option>
          <option value"DOMINGO">DOMINGO</option>
      </select>
  </div>
  <div class="col-md-4 text-center">
      <label for="">Ingrese la hora:
          <span class="obligatorio">(Obligatorio)</span>
      </label>
      <br>
      <input type="text" placeholder="Ingrese la hora" class="form-control" name="hora_hor" value="<?php echo $horarioEditar->hora_hor; ?>"
      id="hora_hor"required>
      <br>
  </div>

  <div class="col-md-4 text-center">
      <label for="">Ingrese el tipo de ejercicio:
          <span class="obligatorio">(Obligatorio)</span>
      </label>
      <br>
      <input type="text" placeholder="Ingrese el tipo de ejercicio" class="form-control" name="tipo_hor" value="<?php echo $horarioEditar->tipo_hor; ?>"
      id="tipo_hor"required>
      <br>
   </div>
  </div>

  <div class="row">
      <div class="col-md-12 text-center">
          <br>
          <br>
          <button type="submit" name="button" class="btn btn-primary">Actualizar</button>
          <br>
          <br>
          <a href="<?php echo site_url(); ?>/horarios/index"class="btn btn-danger">
          Cancelar</a>
      </div>
   </div>
  </div>
  <br>
  <br>
  <br>
  </form>
        <script type="text/javascript">
        $("#frm_nuevo_horario").validate({
          rules:{
            dia_hor:{
              required:true,

            },
            hora_hor:{
              required:true,
              minlength:2,
              maxlength:5,

            },
            tipo_hor:{
              required:true,
              minlength:2,
              maxlength:50,
              letras:true
            },

          },
          messages:{
            dia_hor:{
              required:"Ingrese el día por favor",

            },
            hora_hor:{
              required:"Ingrese la hora por favor",
              minlength:"Escriba mas de 3 caracteres",
              maxlength:"incorrecto",

            },
            tipo_hor:{
              required:"Ingrese el tipo de ejercicio por favor",
              minlength:"Escriba mas de 2 caracteres",
              maxlength:"incorrecto",
              letras:"Este campo acepta solo letras"
            },

          }
          });
          </script>
