<?php if ($empleados):?>
    <table class="table table-hover table-bordered">
        <thead>
        <h1><b><i>Listado de proveedores</i></b></h1>
            <tr>
                <th>ID</th>
                <th>CEDULA</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>DIRECCION</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($equipos as $filaTemporal): ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->id_prov; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->cedula_prov; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->nombre_prov; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->apellido_prov; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->direccion_prov; ?>
                    </td>

            </tr>
            <?php endforeach; ?>
          </tbody>
