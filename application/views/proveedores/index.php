<br>
<br>
<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">Listado Proveedores</p> </b></h1></div>
    </div>
    <div class="col-md-12 text-center"> <br>
        <a href="<?php echo site_url('proveedores/nuevo'); ?>" class="btn btn-success">
            <i class="glyphicon glyphicon-plus"></i>
            Agregar Proveedor
       </a>
    </div>
    <br>
    <br>
    <br>
  <?php if ($listadoProveedores): ?>
<table class="table table-striped table-bordered table-hover" id="tbl_proveedores">
  <thead>
    <tr>
      <th class="text-center">ID</th>
      <th class="text-center">CEDULA</th>
      <th class="text-center">NOMBRE</th>
      <th class="text-center">APELLIDO</th>
      <th class="text-center">DIRECCION</th>
      <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($listadoProveedores as $Temporal): ?>
      <tr>
        <td class="text-center"><?php echo  $Temporal->id_prov ?></td>
        <td class="text-center"><?php echo $Temporal->cedula_prov?></td>
        <td class="text-center"><?php echo  $Temporal->nombre_prov ?></td>
        <td class="text-center"><?php echo $Temporal->apellido_prov ?></td>
        <td class="text-center"><?php echo $Temporal->direccion_prov ?></td>
        <td class="text-center">
          <a  href="<?php echo site_url("proveedores/editar");?>/<?php echo $Temporal->id_prov ; ?> "class="btn btn-danger"
          <i class="glyphicon glyphicon-trash" ></i>Editar</a>
          </button>
          <?php if ($this->session->userdata("conectado")->perfil_usu == "ADMINISTRADOR"): ?>
                            <a href="<?php echo site_url(); ?>/proveedores/eliminar/<?php echo $Temporal->id_prov; ?>"class="btn btn-warning"
                            title="Eliminar Proveedor"
                            onclick="return confirm('¿Esta seguro?');"
                            style="color:red;">
                            <i class="mdi mdi-close"></i>Eliminar</a>
                  <?php endif; ?>
           <!-- <a href="<?php echo site_url("proveedores/borrar") ?>/<?php echo $Temporal->id_prov ; ?>" class="btn btn-warning"
             title="Eliminar Proveedor"
             onclick="return confirm('Esta seguro de eliminar Proveedor');"></i>Eliminar</a> -->
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  </table>
<?php else: ?>
  <h3><b>No existe proveedores</b></h3>
<?php endif; ?>
<script type="text/javascript">
    $("#tbl_proveedores")
    .DataTable();
</script>

  <!-- <script type="text/javascript">
    $("#tabla-postulaciones").DataTable();
  </script> -->
  <br>
<br>
<br>
<br>
