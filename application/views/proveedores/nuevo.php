<div class="row">
    <div class="col-md-12 text-center">
    <h1><b><p style="color:#001f36;">INGRESE UN NUEVO PROVEEDOR </p> </b></h1></div>
    </div>
</div>
<br>
<br>
<form class="" id="frm_nuevo_proveedor"action="<?php echo site_url("proveedores/guardarProveedores"); ?>" method="post">
<div class="container">
        <div class="row">
          <div class="col-md-4 text-center">
              <label for="">Ingrese la Cedula:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="number" placeholder="Ingrese su cedula" class="form-control" name="cedula_prov" value=""
              id="cedula_prov" required>
              <br>
          </div>
          <div class="col-md-4 text-center">
              <label for="">Ingrese el Nombre:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese su nombre" class="form-control" name="nombre_prov" value=""
              id="nombre_prov"required>
              <br>
          </div>

          <div class="col-md-4 text-center">
              <label for="">Ingrese el apellido:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese su apellido" class="form-control" name="apellido_prov" value=""
              id="apellido_prov"required>
              <br>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
              <label for="">Ingrese su direccion:
                  <span class="obligatorio">(Obligatorio)</span>
              </label>
              <br>
              <input type="text" placeholder="Ingrese su direccion" class="form-control" name="direccion_prov" value=""
              id="direccion_prov"required>
              <br>
          </div>
        </div>
</div>
        <div class="row">
            <div class="col-md-12 text-center">
                <br>
                <br>
                <button type="submit" name="button" class="btn btn-primary">Guardar</button>
                <a href="<?php echo site_url(); ?>/proveedores/listas"class="btn btn-danger">
                Cancelar</a>
            </div>
        </div>
        <br>
</form>
<br>
<br>

<script type="text/javascript">
$("#frm_nuevo_proveedor").validate({
rules:{
  cedula_prov:{
    required:true,
    minlength:3,
    maxlength:250,
    digits:true
  },
  nombre_prov:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  apellido_prov:{
    required:true,
    minlength:3,
    maxlength:250,
    letras:true
  },
  direccion_prov:{
    required:true,
    minlength:2,
    maxlength:250,
    letras:true
  },

},
messages:{
  cedula_prov:{
    required:"Ingrese su cedula por favor",
    minlength:"Ingrese al menos de 10 digitos",
    maxlength:"cedula incorrecta",
    digits:"Este campo acepta solo numeros"
  },
  nombre_prov:{
    required:"Ingrese su nombre por favor",
    minlength:"Escriba mas de 3 caracteres",
    maxlength:"Nombre incorrecto",
    letras:"Este campo acepta solo letras"
  },
  apellido_prov:{
    required:"Ingrese su apellido por favor",
    minlength:"Ingrese al menos 3 digito",
    maxlength:"Apellido incorrecto",
    letras:"Este campo solo acepta letras"
  },
  direccion_prov:{
    required:"Ingrese su direccion por favor",
    minlength:"Escriba mas de 3 caracteres",
    maxlength:"Direccion incorrecto",
    letras:"Este campo acepta solo letras"
  },

}
});
</script>
